import secrets
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand

from app.models import Event

secrets_Generator = secrets.SystemRandom()


def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


def generate_unique_id() -> int:
    return secrets_Generator.randrange(100000, 999999)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--start-date",
            type=str,
            help="Start of event date range"
        )
        parser.add_argument(
            "--end-date",
            type=str,
            help="End of event date range"
        )

    def handle(self, *args, **kwargs):
        start_date = datetime.strptime(kwargs["start_date"], "%Y-%m-%d")
        end_date = datetime.strptime(kwargs["end_date"], "%Y-%m-%d")

        datetime_range = int((end_date - start_date).days + 1)
        for date in (start_date + timedelta(n) for n in range(datetime_range)):
            Event.objects.create(
                unique_id=f"440AS-{generate_unique_id()}",
                aircraft="440AS",
                datetime=date
            )
