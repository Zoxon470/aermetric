from django.db import models


class Chronicle(models.Model):
    STATUS_OPEN = "Open"
    STATUS_CLOSE = "Close"
    STATUS_CHOICES = (
        (STATUS_OPEN, STATUS_OPEN),
        (STATUS_CLOSE, STATUS_CLOSE),
    )

    unique_id = models.CharField(max_length=12, unique=True)
    aircraft = models.CharField(max_length=5)
    status = models.CharField(
        max_length=5,
        choices=STATUS_CHOICES,
        default=STATUS_OPEN
    )
    min_timestamp = models.DateTimeField()
    max_timestamp = models.DateTimeField()

    def __str__(self):
        return self.unique_id


class Event(models.Model):
    unique_id = models.CharField(max_length=12, unique=True)
    aircraft = models.CharField(max_length=5)
    datetime = models.DateTimeField()

    def __str__(self):
        return self.unique_id
