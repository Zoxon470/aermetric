from datetime import datetime

from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Chronicle


class EventAPIView(APIView):
    def get(self, *args, **kwargs):
        min_timestamp = self.request.query_params.get(
            "min_timestamp", "2021-05-24"
        )
        max_timestamp = self.request.query_params.get(
            "max_timestamp", "2021-05-25"
        )
        query = f"""
                SELECT app_chronicle.id, app_chronicle.min_timestamp, 
                app_chronicle.max_timestamp,
                       app_chronicle.aircraft,
                       app_chronicle.status,
                       app_chronicle.unique_id,
                       app_event.datetime
                FROM app_chronicle
                INNER JOIN app_event ON app_chronicle.aircraft = app_event.aircraft
                WHERE app_event.datetime BETWEEN '{min_timestamp}' and '{max_timestamp}'
                GROUP BY app_chronicle.id, app_event.datetime;
            """

        events = Chronicle.objects.raw(query)
        chronicle = events[0]
        dt_min_timestamp = datetime.strptime(min_timestamp, "%Y-%m-%d")
        dt_max_timestamp = datetime.strptime(max_timestamp, "%Y-%m-%d")
        chronicle_days_delta = dt_max_timestamp.day - dt_min_timestamp.day

        chronicle_events_list = []
        for idx, day in enumerate(range(1, chronicle_days_delta + 2)):
            chronicle_events_list.append(0)
            for event in events:
                if event.datetime.day == dt_min_timestamp.day - 1 + day:
                    chronicle_events_list[idx] = 1

        return Response({
            "min_timestamp": chronicle.min_timestamp,
            "max_timestamp": chronicle.max_timestamp,
            "aircraft": chronicle.aircraft,
            "status": chronicle.status,
            "unique_id": chronicle.unique_id,
            "chronicle_events": chronicle_events_list
        })
