from django.urls import path

from app.views import EventAPIView

urlpatterns = [
    path("events", EventAPIView.as_view())
]
