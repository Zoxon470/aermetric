.PHONY: migrate
migrate:
	@echo "Migrating database..."
	python manage.py migrate --noinput

.PHONY: run
run: migrate
	@echo "Running server..."
	python manage.py runserver 0.0.0.0:8000


