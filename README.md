# Aermetric

### How to reproduce a task
```sh
$ docker-compose up --build -d # for pull images and start
$ docker exec -ti aermetric-app ./manage.py upload_events --start-date 2021-05-20 --end-date 2021-05-21 # for upload events from date range --start-date and --end-date
$ curl -H "Accept: application/json" "http://127.0.0.1:8000/events?min_timestamp=2021-05-24&max_timestamp=2021-05-25" | jq
```
